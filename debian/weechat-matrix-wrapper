#!/bin/sh
# Enable and disable Weechat-matrix installed from Debian package
set -e

DESTINATION=~/.weechat/python
SOURCE=/usr/lib/$( gcc -dumpmachine )/weechat/plugins/python

showhelp () {
    echo "Usage: weechat-matrix-wrapper [enable|disable]
    enable
        Create the symlinks to enable Weechat-matrix for the current user
    disable
        Remove the symlinks for Weechat-matrix for the current user
    "
    exit 0
}

ACTION=$1

if [ "$ACTION" = "enable" ]; then
    echo "Enabling Weechat-matrix"
    if [ -L $DESTINATION/matrix ]; then
        echo "Weechat-matrix was already enabled"
        exit 0
    fi
    if [ -d $DESTINATION/matrix ]; then
        echo "$DESTINATION/matrix exists, unable to continue"
        exit 1
    fi
    ln -s $SOURCE/matrix $DESTINATION/matrix
    ln -s $SOURCE/matrix.py $DESTINATION/autoload/matrix.py
elif [ "$ACTION" = "disable" ]; then
    echo "Disabling Weechat-matrix"
    if [ ! -L $DESTINATION/matrix ]; then
        echo "$DESTINATION/matrix is not a link, unable to continue"
        exit 1
    fi
    if [ ! -L $DESTINATION/autoload/matrix.py ]; then
        echo "$DESTINATION/autoload/matrix.py is not a link, unable to continue"
        exit 1
    fi
    rm -f $DESTINATION/matrix $DESTINATION/autoload/matrix.py
else
    showhelp
fi

exit 0
